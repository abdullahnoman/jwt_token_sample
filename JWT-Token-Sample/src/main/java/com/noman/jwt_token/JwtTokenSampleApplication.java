package com.noman.jwt_token;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JwtTokenSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(JwtTokenSampleApplication.class, args);
	}

}
