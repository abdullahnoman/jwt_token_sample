package com.noman.jwt_token.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @RequestMapping(path = "/hello", method = RequestMethod.GET)
    public String firstPage() {
        return "Hello World";
    }
}
